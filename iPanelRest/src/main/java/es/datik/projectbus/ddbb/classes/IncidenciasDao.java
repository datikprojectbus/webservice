package es.datik.projectbus.ddbb.classes;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import es.datik.projectbus.ddbb.domain.Formulario;
import es.datik.projectbus.ddbb.domain.Incidencia;
import es.datik.projectbus.ddbb.resources.HibernateUtil;

public class IncidenciasDao {
	Logger logger = Logger.getLogger("IncidenciasDao");
	
	public void addIncidencia(Incidencia incidencia) {
        Session session = null; 
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(incidencia);
            session.getTransaction().commit();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"Adding Incidence: " + e.getClass()+ ":" + e.getMessage());
        	session.close();
            throw e;
        }
    }
	
    public void deleteIncidencia(int id){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Incidencia incidencia = (Incidencia)session.createCriteria(Incidencia.class).add(Restrictions.eq("idIncidencia", id)).uniqueResult();            
            session.delete(incidencia);
            session.getTransaction().commit();
            
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Delete incidence: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }
    public List<Incidencia> getAllIncidences() {
       	List<Incidencia> listincidencias = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            listincidencias = session.createCriteria(Incidencia.class).addOrder(Order.asc("idIncidencia")).list();
            session.close();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"Read all incidence: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;}
        return listincidencias;
    }
    public List<Incidencia> getAllIncidencesMecanicas() {
       	List<Incidencia> listincidencias = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            listincidencias = session.createCriteria(Incidencia.class)
            		.add(Restrictions.eq("tipo", "mecanico"))
            		.addOrder(Order.asc("idIncidencia")).list();
            session.close();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"Read all incidence: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;}
        return listincidencias;
    }
}

