package es.datik.projectbus.rest.prueba;

import java.io.IOException;
import java.util.logging.Logger;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

import es.datik.projectbus.dto.HTTPHeaderNames;

@Provider
@PreMatching
public class RestResponseFilter implements ContainerResponseFilter {

    private final static Logger log = Logger.getLogger( RestResponseFilter.class.getName() );
	
    @Override
	public void filter(ContainerRequestContext request, ContainerResponseContext response) throws IOException {
    	 log.info( "Filtering REST Response" );
    	 
         response.getHeaders().add( "Access-Control-Allow-Origin", "*" );   
         response.getHeaders().add( "Access-Control-Allow-Credentials", "true" );
         response.getHeaders().add( "Access-Control-Allow-Methods", "GET, POST, DELETE, PUT" );
         response.getHeaders().add( "Access-Control-Allow-Headers", HTTPHeaderNames.AUTH_TOKEN + "," + HTTPHeaderNames.CONTENT_TYPE );
	}
}
