package es.datik.projectbus.ddbb.classes;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.hibernate.Session;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;


import es.datik.projectbus.ddbb.domain.Users;

import es.datik.projectbus.ddbb.resources.HibernateUtil;

public class UserDao {
	
	Logger logger = Logger.getLogger("UserDao");
    
	public void addUser(Users user) {
        Session session = null; 
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"addUser: " + e.getClass()+ ":" + e.getMessage());
        	session.close();
            throw e;
        }
    }

    public void deleteUser(int id){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Users user = (Users)session.createCriteria(Users.class).add(Restrictions.eq("id", id)).uniqueResult();
            
            session.delete(user);
            session.getTransaction().commit();
            
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Delete user: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }

    public void updateUser(Users user) {
    	Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
        	session.beginTransaction();
        	
            session.update(user);
            session.getTransaction().commit();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Update user: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }
    
    public List<Users> getAllUsers() {
       	List<Users> listusers = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            
            listusers = session.createCriteria(Users.class).addOrder(Order.asc("id")).list(); 
            session.close();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"Read all users: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;}
        return listusers;
    }

    public Users getUserById(int id) {
        Users user = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
			user = (Users)session.createCriteria(Users.class).add(Restrictions.eq("id", id )).uniqueResult();
			session.close();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Get user by id: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
        return user;
    }
    public Users getUserByUsername(String username) {
        Users user = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
			user = (Users)session.createCriteria(Users.class).add(Restrictions.eq("username", username )).uniqueResult();
			session.close();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Get user by id: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
        return user;
    }
}
