package es.datik.projectbus.dto;

import java.util.Calendar;
import java.util.Date;

import javax.security.auth.login.LoginException;

import es.datik.projectbus.ddbb.classes.SesionesDao;
import es.datik.projectbus.ddbb.classes.UserDao;
import es.datik.projectbus.ddbb.domain.Sesiones;
import es.datik.projectbus.ddbb.domain.Users;
import es.datik.projectbus.utils.EncodeBase64;
import es.datik.projectbus.utils.Security;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class DemoAuthenticator {

	private static DemoAuthenticator authenticator = null;
	
    public static DemoAuthenticator getInstance() {
        if ( authenticator == null ) {
            authenticator = new DemoAuthenticator();
        }
 
        return authenticator;
    }
    /**
     * This is the method that will check the username and password, and return the auth token if it's
     * valid.
     * @param username
     * @param password
     * @return
     * @throws LoginException
     */
    public String login(String username, String password ) throws LoginException {
        
    	UserDao dao = new UserDao();
    	Users user = dao.getUserByUsername(username);
        Security security = new Security();
        String hash = security.md5(password);
    	if (user.getUsername().equals(username) && user.getPassword().equals(hash)){
            /**
             * Once all params are matched, the authToken will be
             * generated and will be stored in the database.
             */
    		SesionesDao daosesiones = new SesionesDao();
    		int iduser = user.getId();
		 	Date dt = new Date();
		 	Token token = new Token(iduser, user.getUsername(), user.getPrivilege(), dt.getTime());
		 	String authToken = "";
		 	try {
		 		authToken = EncodeBase64.serialize(token);
		 	} catch (IOException e) {
		 		e.printStackTrace();
		 		throw new LoginException( "Error parsing de token class: " + e );
		 	}
		 	Sesiones users = daosesiones.getUserById(iduser);
		 	if (users == null){
		 		Sesiones usersesion = new Sesiones();
			    usersesion.setUsers(user);
			    usersesion.setFecha(dt);
			    usersesion.setToken(authToken);
			    Calendar calendar = Calendar.getInstance();
			    calendar.add(Calendar.HOUR, 1);
			    dt = calendar.getTime();
			    usersesion.setTokenExpiration(dt);
			    daosesiones.addUserSesion(usersesion);
		 	}
		 	else{
			 	users.setToken(authToken);
			    users.setFecha(dt);
			    Calendar calendar = Calendar.getInstance();
			    calendar.add(Calendar.HOUR, 1);
			    dt = calendar.getTime();
			    users.setTokenExpiration(dt);
			    daosesiones.updateUserSesion(users); 
		 	}
    		return authToken;
            }
    	
    	throw new LoginException( "Don't Come Here Again!" );
    }
    
    /**
     * The method that pre-validates if the client which invokes the REST API is
     * from a authenticated source = authtoken.
     * @param authToken The authorization token generated after login
     * @return TRUE for acceptance and FALSE for denied.
     */
    public boolean isAuthTokenValid(String authToken ) {
    	SesionesDao dao2 = new SesionesDao();
        Sesiones usersesion = dao2.getUserByToken(authToken);
    	if (usersesion.getToken().equals(authToken)) {
    		return true;
        }	
    	return false;
    }
    
  // Method that checks whether the token has expired.
    public boolean isTokenExpired(String authToken ) {
    	SesionesDao dao2 = new SesionesDao();
        Sesiones usersesion = dao2.getUserByToken(authToken);
        Calendar cal1 = Calendar.getInstance();
        long millis1 = cal1.getTimeInMillis();
        cal1.setTime(usersesion.getTokenExpiration());
        long millis2 = cal1.getTimeInMillis();
    	if ( millis1 >= millis2) {
    		return true;
        }
    	// Here, we add more life (30') to the token.
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 30);
        Date new_time = calendar.getTime();
        usersesion.setTokenExpiration(new_time);
		return false;
    }
    
    /**
     * When a client logs out, the authentication token will be removed from the db 
     * and will be made invalid.
     */
    
    public void logout(String authToken ) throws GeneralSecurityException {
        if ( isAuthTokenValid (authToken) ) {
        	SesionesDao dao2 = new SesionesDao();
        	Sesiones usersesion = dao2.getUserByToken(authToken);
        	int userid = usersesion.getId();
		 	dao2.deleteUserSesion(userid);
        	return;
        }
        throw new GeneralSecurityException( "Invalid authorization token match." );
    }
}
 

