package es.datik.projectbus.ddbb.classes;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import es.datik.projectbus.ddbb.domain.Checklist;
import es.datik.projectbus.ddbb.resources.HibernateUtil;

public class ChecklistDao {
	
	Logger logger = Logger.getLogger("ChecklistDao");

	public void addChecklist(Checklist checklist) {
        Session session = null; 
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(checklist);
            session.getTransaction().commit();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"addChecklist: " + e.getClass()+ ":" + e.getMessage());
        	session.close();
            throw e;
        }
    }

    public void deleteChecklist(int id){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Checklist checklist = (Checklist)session.createCriteria(Checklist.class).add(Restrictions.eq("idchecklist",id)).uniqueResult();
            session.delete(checklist);
            session.getTransaction().commit();
            
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Delete checklist: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }

    public Checklist getChecklistbyMatriculaFecha(String matricula, Date fecha) {
        Checklist checklist = null;
    	Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            checklist = (Checklist)session.createCriteria(Checklist.class)
            		.add(Restrictions.eq("vehiculo.matricula", matricula))
            		.add(Restrictions.eq("fecha",fecha)).uniqueResult();
            session.close();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Get checklist by matricula and fecha: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
        return checklist;
    }

    public List<Checklist> getChecklistbetween2Dates(String matricula, Date minDate, Date maxDate) {
       	List<Checklist> listchecklist = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Conjunction and = Restrictions.conjunction();
            listchecklist = session.createCriteria(Checklist.class)
            		.add( Restrictions.eq("vehiculo.matricula", matricula))
            		.add( Restrictions.ge("fecha", minDate))
            		.add( Restrictions.lt("fecha", maxDate))
            		.addOrder( Order.asc("fecha")).list();
            session.close();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"Get checklist between 2 dates: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;}
        return listchecklist;
    }
}
