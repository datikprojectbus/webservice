package es.datik.projectbus.dto;

// It allow us to pass the service key and authtoken as a http header instead of as a parameter.
public interface HTTPHeaderNames {
    public static final String AUTH_TOKEN = "auth_token";
    public static final String CONTENT_TYPE = "content-type";
}
