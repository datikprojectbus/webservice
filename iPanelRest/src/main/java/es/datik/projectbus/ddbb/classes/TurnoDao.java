package es.datik.projectbus.ddbb.classes;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import es.datik.projectbus.ddbb.domain.Turno;
import es.datik.projectbus.ddbb.resources.HibernateUtil;

public class TurnoDao {
Logger logger = Logger.getLogger("TurnoDao");
    
	public void addTurno(Turno turno) {
        Session session = null; 
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(turno);
            session.getTransaction().commit();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"addTurno: " + e.getClass()+ ":" + e.getMessage());
        	session.close();
            throw e;
        }
    }
    public void updateTurno(Turno turno) {
    	Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
        	session.beginTransaction();
        	
            session.update(turno);
            session.getTransaction().commit();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Update turno: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }

    public void deleteTurno(String idturno){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Turno turno = (Turno)session.createCriteria(Turno.class).add(Restrictions.eq("idturno", idturno)).uniqueResult();
            
            session.delete(turno);
            session.getTransaction().commit();
            
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Delete turno: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }
    
    public List<Turno> getAllTurnos() {
       	List<Turno> listturnos = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            
            listturnos = session.createCriteria(Turno.class).addOrder(Order.asc("idturno")).list(); 
            session.close();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"Read all turnos: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;}
        return listturnos;
    }
    public Turno getTurnoById(String idturno) {
        Turno turno = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
			turno = (Turno)session.createCriteria(Turno.class).add(Restrictions.eq("idturno", idturno )).uniqueResult();
			session.close();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Get turno by id: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
        return turno;
    }
}
