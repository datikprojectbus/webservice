package es.datik.projectbus.dto;

public class Turno_class {
	private String idturno;
	private String ciudad;
	private String periodo;

	public Turno_class(String idturno, String ciudad, String periodo){
		this.idturno = idturno;
		this.ciudad = ciudad;
		this.periodo = periodo;
	}
}
