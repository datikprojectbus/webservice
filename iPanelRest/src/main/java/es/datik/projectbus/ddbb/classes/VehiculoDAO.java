package es.datik.projectbus.ddbb.classes;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import es.datik.projectbus.ddbb.domain.Users;
import es.datik.projectbus.ddbb.domain.Vehiculo;
import es.datik.projectbus.ddbb.resources.HibernateUtil;

public class VehiculoDAO {

	Logger logger = Logger.getLogger("VehiculoDao");
	
	public List<Vehiculo> readAll(){
		List<Vehiculo> listaVehiculos = null;	
		Session session = null;
		try {				
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			
			listaVehiculos = session.createCriteria(Vehiculo.class).addOrder(Order.asc("matricula")).list();
			
			session.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Read all: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
		}
		return listaVehiculos;
   }
	
	public Vehiculo getElementByMatricula(String matricula){
		Vehiculo vehiculo = null;
		Session session = null;
		try{	
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		
			vehiculo = (Vehiculo)session.createCriteria(Vehiculo.class)
										.add(Restrictions.eq("matricula", matricula ))					
										.uniqueResult();
			
			session.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Get element by matricula: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
		}
		return vehiculo;
	}
	
	public void addVehiculo(Vehiculo vehiculo){
		Session session = null;
		try{		
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			
			session.save(vehiculo);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Insert record: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
		}
	}
    public void deleteVehiculo(String matricula){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Vehiculo vehiculo = (Vehiculo)session.createCriteria(Vehiculo.class).add(Restrictions.eq("matricula", matricula)).uniqueResult();
            
            session.delete(vehiculo);
            session.getTransaction().commit();
            
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Delete vehículo: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }
}
