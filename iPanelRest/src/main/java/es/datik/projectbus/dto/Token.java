package es.datik.projectbus.dto;

public class Token {
	private int idUsuario = 0;
	private String username = null;
	String privilege = null;
	private long startTime = 0;
	
	public Token(){}
	
	public Token(int idUsuario, String username, String privilege,
			 long startTime) {
		super();
		this.idUsuario = idUsuario;
		this.username = username;
		this.privilege = privilege;
		this.startTime = startTime;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPrivilege() {
		return privilege;
	}	
	
	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
}
