package es.datik.projectbus.dto;

import java.util.Date;

public class Checklist_class {
    private int Idchecklist; 
	private String Matricula;
	private Date Fecha;
	private String Turno;
    private int Iduser;
    private String Estadoruedas;
    private String Adblue;
    private String Aire;
    private String Botiquin; 
    private String Calefaccion;
    private String Carroceria;
    private String Correas;
    private String Extintores; 
    private String Libroreclamaciones; 
    private String Libroruta;
    private String Maquinacobro;
    private String Martillos; 
    private String Nivelaceite;
    private String Nivelagua;
    private String Nivelcarburante; 
    private String Rampaminusvalidos; 	
    private String Sepiolita; 	

	public Checklist_class( int Idchecklist, String Matricula, Date Fecha, String Turno, int Iduser, String Estadoruedas, String Adblue,
			String Aire, String Botiquin, String Calefaccion,String Carroceria,String Correas, String Extintores, 
			String Libroreclamaciones, String Libroruta, String Maquinacobro, String Martillos, String Nivelaceite, String Nivelagua,
			String Nivelcarburante, String Rampaminusvalidos, String Sepiolita ){
		this.Idchecklist = Idchecklist;
		this.Matricula = Matricula;
		this.Fecha = Fecha;
		this.Turno = Turno;
		this.Iduser = Iduser;
		this.Estadoruedas = Estadoruedas;
		this.Adblue = Adblue;
		this.Aire = Aire;
		this.Botiquin = Botiquin;
		this.Calefaccion = Calefaccion;
		this.Carroceria = Carroceria;
		this.Correas = Correas;
		this.Extintores = Extintores;
		this.Libroreclamaciones = Libroreclamaciones;
		this.Libroruta = Libroruta;
		this.Maquinacobro = Maquinacobro;
		this.Martillos = Martillos;
		this.Nivelaceite = Nivelaceite;
		this.Nivelagua = Nivelagua;
		this.Nivelcarburante = Nivelcarburante;
		this.Rampaminusvalidos = Rampaminusvalidos;
		this.Sepiolita = Sepiolita;
	}
}
