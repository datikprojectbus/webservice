package es.datik.projectbus.controllers;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import es.datik.projectbus.ddbb.classes.TurnoDao;
import es.datik.projectbus.ddbb.domain.Turno;
import es.datik.projectbus.dto.Turno_class;


public class TurnoGestion {
	private static TurnoGestion instance = null;
    
	public static TurnoGestion getInstance() {
        if (instance == null ) {
            instance = new TurnoGestion();
        }
        return instance;
    }
	//adding a turno to the db.
	public void addTurno(String idturno, String ciudad, String periodo){
		TurnoDao dao = new TurnoDao();
		Turno turno = new Turno();
		turno.setIdturno(idturno);
		turno.setCiudad(ciudad);
		turno.setPeriodo(periodo);
		dao.addTurno(turno);
	}
	//updating a turno from the db.
	public void updateTurno(String idturno, String ciudad, String periodo){
		TurnoDao dao = new TurnoDao();
		Turno turno = dao.getTurnoById(idturno);
		turno.setCiudad(ciudad);
		turno.setPeriodo(periodo);
		dao.updateTurno(turno);
	}
	//It return a list of all the turnos stored in the db.
	public List<String> readAllTurnos(){
		TurnoDao dao = new TurnoDao();
		final Gson gson = new Gson();
		List<Turno> turnos = dao.getAllTurnos();
		int size = turnos.size();
		List<String> listjson = new ArrayList<String>();
		for (int i=0; i<size; i++)
		{	Turno prueba = turnos.get(i);
			String idturno = prueba.getIdturno();
			String ciudad = prueba.getCiudad();
			String periodo = prueba.getPeriodo();
			Turno_class clase = new Turno_class(idturno,ciudad,periodo);
			String jsonstring = gson.toJson(clase);
			listjson.add(jsonstring);
		}
		return listjson;
	}
	//Deleting a turno
	public void deleteTurno(String idturno){
		TurnoDao dao = new TurnoDao();
		dao.deleteTurno(idturno);
	}
}
