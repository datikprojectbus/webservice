package es.datik.projectbus.rest.prueba;

import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.security.auth.login.LoginException;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ejb.Local;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import es.datik.projectbus.controllers.ChecklistGestion;
import es.datik.projectbus.controllers.FormularioGestion;
import es.datik.projectbus.controllers.IncidenciasGestion;
import es.datik.projectbus.controllers.TurnoGestion;
import es.datik.projectbus.controllers.UsuariosGestion;
import es.datik.projectbus.controllers.VehiculoGestion;
import es.datik.projectbus.dto.DemoAuthenticator;
import es.datik.projectbus.dto.HTTPHeaderNames;

@Local
@Path( "proxyresource" )
public class RestResource{


	@POST
	@Path( "/login" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response login( @Context HttpHeaders httpHeaders,
	        @FormParam( "username" ) String username,
	        @FormParam( "password" ) String password ) {
		DemoAuthenticator authenticator = DemoAuthenticator.getInstance();
		
		try {
			//The auth token also contains privilege for the moment
			 String authToken = authenticator.login( username, password );
			 
			 JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
	         jsonObjBuilder.add( "auth_token", authToken );
	         JsonObject data = jsonObjBuilder.build();
	         return getNoCacheResponseBuilder( Response.Status.OK ).entity( data.toString() ).build();
		 } catch ( final LoginException ex ) {
	         JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
	         jsonObjBuilder.add( "message", "Problem matching username and password" );
	         JsonObject data = jsonObjBuilder.build();
	 
	         return getNoCacheResponseBuilder( Response.Status.UNAUTHORIZED ).entity( data.toString() ).build();
		 }
	}

	@GET
	@Path( "/get-userslist" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response GetUsersMethod() {
		UsuariosGestion userobject = UsuariosGestion.getInstance();
		List<String> Jsondata = userobject.GetAllUsers();
 
        return getNoCacheResponseBuilder( Response.Status.OK ).entity( Jsondata.toString() ).build();
    }
	
	@GET
	@Path( "/get-turnos" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response GetTurnosMethod() {
		TurnoGestion turnobject = TurnoGestion.getInstance();
		List<String> Jsondata = turnobject.readAllTurnos();
        return getNoCacheResponseBuilder( Response.Status.OK ).entity( Jsondata.toString() ).build();
    }
	
	@GET
	@Path( "/get-vehiculos" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response GetVehiculosMethod() {
		VehiculoGestion vehiculobject = VehiculoGestion.getInstance();
		List<String> Jsondata = vehiculobject.readAllVehiculos();
        return getNoCacheResponseBuilder( Response.Status.OK ).entity( Jsondata.toString() ).build();
    }
	
	@GET
	@Path( "/get-incidencias" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response GetIncidenciasMethod() {
		IncidenciasGestion incidenciasobject = IncidenciasGestion.getInstance();
		List<String> Jsondata = incidenciasobject.getIncidencias();
        return getNoCacheResponseBuilder( Response.Status.OK ).entity( Jsondata.toString() ).build();
    }
	@GET
	@Path( "/get-incidenciasmecanicas" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response GetIncidenciasMecanicasMethod() {
		IncidenciasGestion incidenciasobject = IncidenciasGestion.getInstance();
		List<String> Jsondata = incidenciasobject.getIncidenciasMecanicas();
        return getNoCacheResponseBuilder( Response.Status.OK ).entity( Jsondata.toString() ).build();
    }
	
	@GET
	@Path( "/get-formulario" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response GetMethod() {
		FormularioGestion formulariobject = FormularioGestion.getInstance();
		String Jsondata = formulariobject.getAllFormulario();
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "listformulario", Jsondata );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.OK ).entity( data.toString() ).build();
    }

	@POST
	@Path( "/get-info" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response Getinfo( @FormParam( "matricula" ) String matricula, @FormParam( "startDate" ) String startDate, @FormParam( "endDate" ) String endDate) throws ParseException {
		ChecklistGestion checklistobject = ChecklistGestion.getInstance();
		List<String> Jsondata = checklistobject.getInfo(matricula, startDate, endDate);
 
        return getNoCacheResponseBuilder( Response.Status.OK ).entity( Jsondata.toString() ).build();
    }

	@POST
	@Path( "/post-adduser" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostAddUser( @FormParam( "firstName" ) String firstName, @FormParam( "lastName" ) String lastName,
		@FormParam( "username" ) String username, @FormParam( "privilege" ) String privilege, @FormParam( "email" ) String email, @FormParam( "password" ) String password) throws LoginException {
		UsuariosGestion userobject = UsuariosGestion.getInstance();
		userobject.gestionusuarios(firstName, lastName, username, privilege, email, password);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostUserMethod" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	
	@POST
	@Path( "/post-updateuser" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostUpdateUser( @FormParam( "firstName" ) String firstName, @FormParam( "lastName" ) String lastName,
		@FormParam( "username" ) String username, @FormParam( "privilege" ) String privilege, @FormParam( "email" ) String email, @FormParam( "password" ) String password) throws LoginException {
		UsuariosGestion userobject = UsuariosGestion.getInstance();
		userobject.gestionusuarios(firstName, lastName, username, privilege, email, password);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostUpdateUserMethod" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	@POST
	@Path( "/post-deleteuser" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostDeleteUser( @FormParam( "id" ) int id) throws LoginException {
		UsuariosGestion userobject = UsuariosGestion.getInstance();
		userobject.DeleteUser(id);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostDeleteUser" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	
	@POST
	@Path( "/post-addturno" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostCreateTurno( @FormParam( "idturno" ) String idturno, @FormParam( "ciudad" ) String ciudad,
			@FormParam( "periodo" ) String periodo) throws LoginException {
		TurnoGestion turnobject = TurnoGestion.getInstance();
		turnobject.addTurno(idturno,ciudad,periodo);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostAddTurno" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	@POST
	@Path( "/post-updateturno" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostUpdateTurno( @FormParam( "idturno" ) String idturno, @FormParam( "ciudad" ) String ciudad , @FormParam( "periodo" ) String periodo) throws LoginException {
		TurnoGestion turnobject = TurnoGestion.getInstance();
		turnobject.updateTurno(idturno, ciudad, periodo);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostUpdateUpdateMethod" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	
	@POST
	@Path( "/post-deleteturno" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostDeleteTurno( @FormParam( "idturno" ) String idturno) throws LoginException {
		TurnoGestion turnobject = TurnoGestion.getInstance();
		turnobject.deleteTurno(idturno);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostDeleteTurno" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	
	@POST
	@Path( "/post-addvehiculo" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostAddVehiculo( @FormParam( "matricula" ) String matricula, @FormParam( "bodywork" ) String bodywork,
			@FormParam( "modelo" ) String modelo, @FormParam( "motor" ) String motor) throws LoginException {
		VehiculoGestion vehiculobject = VehiculoGestion.getInstance();
		vehiculobject.addVehiculo(matricula, bodywork, modelo, motor);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostAddVehiculo" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	
	@POST
	@Path( "/post-deletevehiculo" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostDeleteVehiculo( @FormParam( "matricula" ) String matricula) throws LoginException {
		System.out.print(matricula);
		VehiculoGestion vehiculobject = VehiculoGestion.getInstance();
		vehiculobject.deleteVehiculo(matricula);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostDeleteVehiculo" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	
	@POST
	@Path( "/post-addformulario" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostMethod( @FormParam("nombre") String nombre, @FormParam("aceite") boolean nivelaceite, @FormParam("agua") boolean nivelagua, @FormParam("carburante")boolean nivelcarburante,
			@FormParam("ruedas") boolean estadoruedas, @FormParam("carroceria") boolean carroceria, @FormParam("rampa") boolean rampa, @FormParam("adblue")boolean adblue, @FormParam("maquina") boolean maquina, @FormParam("aire") boolean aire,
			@FormParam("calefaccion") boolean calefaccion, @FormParam("extintores") boolean extintores, @FormParam("libroreclamaciones") boolean libroreclamaciones, @FormParam("libroruta") boolean libroruta, @FormParam("botiquin") boolean botiquin, 
			@FormParam("martillos") boolean martillos,@FormParam("correas") boolean correas, @FormParam("sepiolita") boolean sepiolita) throws LoginException {
		FormularioGestion formulariobject = FormularioGestion.getInstance();
		formulariobject.addFormulario(nombre, nivelaceite, nivelagua, nivelcarburante, estadoruedas, carroceria, rampa, adblue, maquina, aire, calefaccion, extintores, libroreclamaciones, libroruta, botiquin, martillos, correas, sepiolita);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed AddFormulario" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	
	@POST
	@Path( "/post-updateformulario" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response UpdateFormulario (@FormParam("nombre") String nombre, @FormParam("aceite") boolean nivelaceite, @FormParam("agua") boolean nivelagua, @FormParam("carburante")boolean nivelcarburante,
			@FormParam("ruedas") boolean estadoruedas, @FormParam("carroceria") boolean carroceria, @FormParam("rampa") boolean rampa, @FormParam("adblue")boolean adblue, @FormParam("maquina") boolean maquina, @FormParam("aire") boolean aire,
			@FormParam("calefaccion") boolean calefaccion, @FormParam("extintores") boolean extintores, @FormParam("libroreclamaciones") boolean libroreclamaciones, @FormParam("libroruta") boolean libroruta, @FormParam("botiquin") boolean botiquin, 
			@FormParam("martillos") boolean martillos,@FormParam("correas") boolean correas, @FormParam("sepiolita") boolean sepiolita) throws LoginException {
		FormularioGestion formulariobject = FormularioGestion.getInstance();
		formulariobject.updateFormulario(nombre, nivelaceite, nivelagua, nivelcarburante, estadoruedas, carroceria, rampa, adblue, maquina, aire, calefaccion, extintores, libroreclamaciones, libroruta, botiquin, martillos, correas, sepiolita);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed UpdateFormulario" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
	}
	
	@POST
	@Path( "/post-deleteformulario" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostMethod (@FormParam("nombre") String nombre){
		FormularioGestion formulariobject = FormularioGestion.getInstance();
		formulariobject.deleteFormulario(nombre);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed DeleteFormulario" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
	}
	
	@POST
	@Path( "/post-addincidencia" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response AddIncidencia( @FormParam( "matricula" ) String matricula, @FormParam( "username" ) String username, 
			@FormParam( "turno" ) String turno, @FormParam( "tipo" ) String tipo, @FormParam( "descripcion" ) String descripcion) throws LoginException {
		IncidenciasGestion incidenciaobject = IncidenciasGestion.getInstance();
		incidenciaobject.addincidencias(matricula,username,turno,tipo,descripcion);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed PostIncidencia" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	@POST
	@Path( "/post-deleteincidencia" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response DeleteIncidencia (@FormParam("idincidencia") int id){
		IncidenciasGestion incidenciaobject = IncidenciasGestion.getInstance();
		incidenciaobject.deleteIncidencia(id);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed DeleteIncidencia" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
	}
	
	@POST
	@Path( "/post-addchecklist" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response PostMethod( @FormParam( "matricula" ) String matricula, @FormParam( "username" ) String username, 
			@FormParam( "turno" ) String turno, @FormParam("aceite") String nivelaceite, @FormParam("agua") String nivelagua, @FormParam("carburante") String nivelcarburante,
			@FormParam("ruedas") String estadoruedas, @FormParam("carroceria") String carroceria, @FormParam("rampa") String rampa, @FormParam("adblue") String adblue, @FormParam("maquina") String maquina, @FormParam("aire") String aire,
			@FormParam("calefaccion") String calefaccion, @FormParam("extintores") String extintores, @FormParam("libroreclamaciones") String libroreclamaciones, @FormParam("libroruta") String libroruta, @FormParam("botiquin") String botiquin, 
			@FormParam("martillos") String martillos,@FormParam("correas") String correas, @FormParam("sepiolita") String sepiolita) throws LoginException {
		ChecklistGestion checklistobject = ChecklistGestion.getInstance();
		checklistobject.addChecklist(matricula,username,turno, nivelaceite, nivelagua, nivelcarburante, estadoruedas, carroceria, rampa, adblue, maquina, aire, calefaccion, extintores, libroreclamaciones, libroruta, botiquin, martillos, correas, sepiolita);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed AddChecklist" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
    }
	@POST
	@Path( "/post-deletechecklist" )
	@Produces( MediaType.APPLICATION_JSON )
	public Response DeleteChecklist (@FormParam("idchecklist") int id){
		ChecklistGestion checklistobject = ChecklistGestion.getInstance();
		checklistobject.deleteChecklist(id);
		JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add( "message", "Executed DeleteChecklist" );
        JsonObject data = jsonObjBuilder.build();
 
        return getNoCacheResponseBuilder( Response.Status.ACCEPTED ).entity( data.toString() ).build();
	}
	
	@POST
	@Path( "/logout" )
	public Response logout(@Context HttpHeaders httpHeaders) {
		 try {
			 DemoAuthenticator authenticator = DemoAuthenticator.getInstance();
	         String authToken = httpHeaders.getHeaderString( HTTPHeaderNames.AUTH_TOKEN );
	 
	         authenticator.logout( authToken );
	 
	         return getNoCacheResponseBuilder( Response.Status.NO_CONTENT ).build();
	     } catch ( final GeneralSecurityException ex ) {
	         return getNoCacheResponseBuilder( Response.Status.INTERNAL_SERVER_ERROR ).build();
	     }
	}

	private Response.ResponseBuilder getNoCacheResponseBuilder( Response.Status status ) {
        CacheControl cc = new CacheControl();
        cc.setNoCache( true );
        cc.setMaxAge( -1 ); //means disabled
        cc.setMustRevalidate( true ); 
 
        return Response.status( status ).cacheControl( cc );
    }
}


