package es.datik.projectbus.ddbb.resources;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import es.datik.projectbus.utils.EnvManager;

import java.util.Properties;

public class HibernateUtil {
	
	static Logger logger = Logger.getLogger("HibernateUtil");
	
    private static SessionFactory sessionFactory = buildSessionFactory();
    
    private static Configuration conf = null;
    
    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
        	Configuration configuration = getConfiguration();//new Configuration();

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                    configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            return sessionFactory;
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
			logger.error("#22#500# HibernateUtil buildSessionFactory: " + ex );
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
    	if (sessionFactory == null) {
    		sessionFactory = loadConfiguration();
    	}
        return sessionFactory;
    }
    
    private static  SessionFactory loadConfiguration(){
    	Configuration cfg = getConfiguration();   
    	ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties()).build();       
        sessionFactory = cfg.buildSessionFactory(serviceRegistry);
    	
        return sessionFactory;

    }
    
    public static Configuration getConfiguration() {
    	if (conf == null) {
    		try {
	    		conf = new Configuration();
	        	// Primero se carga la configuración del fichero 
	        	// hibernate_webconfig.cfg.xml ya que es donde están configuradas las 
	        	// clases de mapeo contra la BBDD
	        	conf.configure("es/datik/projectbus/ddbb/resources/hibernate.cfg.xml");
	        	
	        	// Se consiguen los valores de configuración bmontior de webconfig
	        	// Luego se sobreescriben las propiedades del fichero 
	        	// hibernate.properties para poder configurar desde el 
	        	// fichero los datos de conexión
	        	Properties properties = EnvManager.getProperties("projectbus");
	        	conf.setProperties(properties);
	        	// Luego se sobreescriben las propiedades del fichero 
	        	// hibernate.properties para poder configurar desde el 
	        	// fichero los datos de conexión
	            // Properties props = new Properties();
	            
	            logger.info("Configuración cargada");
    		}catch (Exception e) {
    			logger.error("#20#511# Error getConfiguration BD: " + e);
    		}

    	}
		return conf;
	}
    
}