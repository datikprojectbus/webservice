package es.datik.projectbus.ddbb;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import es.datik.projectbus.ddbb.classes.*;
import es.datik.projectbus.ddbb.domain.Checklist;
import es.datik.projectbus.ddbb.domain.Formulario;
import es.datik.projectbus.ddbb.domain.Incidencia;
import es.datik.projectbus.ddbb.domain.Sesiones;
import es.datik.projectbus.ddbb.domain.Turno;
import es.datik.projectbus.ddbb.domain.Users;
import es.datik.projectbus.ddbb.domain.Vehiculo;
import es.datik.projectbus.dto.Checklist_class;
import es.datik.projectbus.dto.User_class;
import es.datik.projectbus.utils.EncodeBase64;
import es.datik.projectbus.utils.Security;

public class App {
	//Method that helps us testing the database connections.
	 public static void main(String[] args) {

	        System.out.println("Testing db");
	        
	        Security security = new Security();
	        String hash = security.md5("prueba");
	        System.out.print(hash);
	        /* System.out.print(hash);
	        Users user = new Users();
	        UserDao dao = new UserDao();
	        user.setName("Arnau");
	        user.setSurname("Comas");
	        user.setUsername("arnau.comas");
	        user.setPrivilege("user");
	        user.setEmail("arnau@gmail.com");
	        user.setPassword(hash);
	        dao.addUser(user);*/
	        // USER ALL OK -GET ALL USERS.
	        /*Users user = new Users();
	        
	        user.setName("Sofia");
	        user.setSurname("Diaz");
	        user.setUsername("sofi.diaz");
	        user.setPrivilege("user");
	        user.setEmail("sofi@gmail.com");
	        user.setPassword("12345");
	        dao.addUser(user);
	        */
	        // Delete user OK
	        //dao.deleteUser(6);
	        
	        //Get username by id Ok
	        //Users userid = dao.getUserById(3);
	        //System.out.println(userid.getUsername());
	        
	        //Get user by username

	        /*Users username = dao.getUserByUsername("jordi.bisbal");
	        System.out.println(username.getUsername());*/
	        
	        //USERSESION ALL OK - Add usersession
	        
	       
	        // Get usersion by token
	        //SesionesDao dao2 = new SesionesDao();
	        /*Sesiones usersesion = dao2.getUserByToken("proba");
	        System.out.println(usersesion.getToken());
		 	*/
		 	
		 	// Delete User sesion
		 	//dao2.deleteUserSesion(1);
		 	
	       
		 	//Update user Sesion. Getuserbyid is not running sometimes( obsolete methods on the stack problem)
		 	/*SesionesDao dao = new SesionesDao();
	        Sesiones prueba = dao.getUserById(1);
	        Date dt = new Date();
	        prueba.setFecha(dt);
	        Calendar calendar = Calendar.getInstance();
	        calendar.add(Calendar.HOUR, 1);
	        dt = calendar.getTime();
	        prueba.setTokenExpiration(dt);
	        dao.updateUserSesion(prueba);
	        //Time time = new Time(01, 00, 00);
	        */
	        //compare times
	        //Calendar cal = Calendar.getInstance();
	        //long millis = cal.getTimeInMillis();
	        //System.out.println(millis);
	        
	        //INCIDENCIAS
	        IncidenciasDao dao3 = new IncidenciasDao();

	        //get all incidencias.

	        //get Incidencia by matricula y fecha.
	        /*Calendar calendar = Calendar.getInstance();
	        calendar.set(2015,05,17,17,48,24);
	        Date time = calendar.getTime();
	        System.out.println(time);
	        */
	        //dao3.getIncidenciaBymatriculafecha("9000ZRH",time);
	        
	        //crear incidencia.
	        /*Incidencia incidencia = new Incidencia();
	        Vehiculo vehiculo = new Vehiculo();
	        vehiculo.setMatricula("9000ZRH");
	        incidencia.setVehiculo(vehiculo);
	        Turno turno = new Turno();
	        turno.setIdturno("IGD10");
	        incidencia.setTurno(turno);
	        Users user = new Users();
	        user.setId(1);
	        Calendar calendar = Calendar.getInstance();
	        Date dt = calendar.getTime();
	        incidencia.setDate(dt);
	        incidencia.setUsers(user);
	        incidencia.setTipo("otro");
	        incidencia.setDescripcion("Una abuela se ha caido del autobús");
	        dao3.addIncidencia(incidencia);*/
	        
	        //delete Incidencias
	        //dao3.deleteIncidencia(2);
	        
	        //FORMULARIO ALL OK
	        //FormularioDao daoprueba = new FormularioDao();
	        //daoprueba.deleteFormulario("form2");
	        /*Formulario formulario = new Formulario();
	        formulario.setNombre("gas");
	        formulario.setAdblue(true);
	        formulario.setNivelAceite(true);
	        formulario.setNivelAgua(true);
	        formulario.setBotiquin(false);
	        daoprueba.addFormulario(formulario);
	        */
	        //Formulario formulario2 = daoprueba.getFormularioByNombre("FormularioTest");
	        //System.out.print(formulario2);
	        //formulario2.setAdblue(true);
	        //daoprueba.updateFormulario(formulario2);
	        
			/*FormularioDao dao10 = new FormularioDao();
			final Gson gson = new Gson();
			List<Formulario> formulario = dao10.getAllFormularios();
			String representacionJSON = gson.toJson(formulario, new TypeToken<ArrayList<Formulario>>(){}.getType());
			System.out.println(representacionJSON);
	        */
	        //CHECKLIST - 2params
	        //ChecklistDao dao2 = new ChecklistDao();
	        //Checklist checklist = new Checklist();
	        /*Vehiculo vehiculo = new Vehiculo();
	        vehiculo.setMatricula("9000ZRH");
	        checklist.setVehiculo(vehiculo);
	        Turno turno = new Turno();
	        turno.setIdturno("IGD10");
	        checklist.setTurno(turno);
	        Users user = new Users();
	        user.setId(1);
	        Calendar calendar = Calendar.getInstance();
	        Date dt = calendar.getTime();
	        checklist.setUsers(user);
	        checklist.setFecha(dt);
	        checklist.setNivelAceite("alto");
	        checklist.setCarroceria(true);
	        dao2.addChecklist(checklist);*/
	        
	        //get Checklist by matricula y fecha.
	        /*ChecklistDao dao2 = new ChecklistDao();
	        Calendar calendar2 = Calendar.getInstance();
			final Gson gson = new Gson();
	        //MESES DEL 00 (ENERO) Al 11 (DICIEMBRE)
	        calendar2.set(2015,04,20,0,0,0);
	        Date time = calendar2.getTime();
	        calendar2.set(2015,04,26,0,0,0);
	        Date time2 = calendar2.getTime();
	        List<Checklist> checklist2 = dao2.getChecklistbetween2Dates("9000ZRH", time, time2);
	        System.out.print(checklist2.get(0).getFecha());
	        System.out.print(checklist2.get(1).getFecha());
	        */
	        
	        //VEHICULO ALL OK
	        /*VehiculoDAO dao10 = new VehiculoDAO();
	        Vehiculo vehiculo = new Vehiculo();
	        /*vehiculo.setMatricula("1029BMZ");
	        vehiculo.setBodywork("test");
	        vehiculo.setModelo("test");
	        vehiculo.setMotor("test");
	        dao10.addVehiculo(vehiculo);
	        Vehiculo si = dao10.getElementByMatricula("1029BMZ");
	        System.out.print(si.getModelo());System.out.print(id);
	        */
	        
	        //TURNO ALL OK
	        /*TurnoDao dao3 = new TurnoDao();
	        Turno turno = new Turno();
	        turno.setIdturno("ZHR2");
	        turno.setCiudad("Zurich");
	        turno.setPeriodo("semanal");
	        //dao3.addTurno(turno);	        
	        Turno turnox = dao3.getTurnoById("ZHR2");
	        List<Turno> turnolist = dao3.getAllTurnos();
	        System.out.print(turnolist);*/
	 }	
}

