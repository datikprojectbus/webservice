package es.datik.projectbus.controllers;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import es.datik.projectbus.ddbb.classes.FormularioDao;
import es.datik.projectbus.ddbb.domain.Formulario;

public class FormularioGestion {

	private static FormularioGestion instance = null;
    
	public static FormularioGestion getInstance() {
        if (instance == null ) {
            instance = new FormularioGestion();
        }
        return instance;
    }
	//adding a form type in the db.
	public void addFormulario(String nombre, boolean nivelaceite, boolean nivelagua, boolean nivelcarburante,
			boolean estadoruedas, boolean carroceria,boolean rampa,boolean adblue, boolean maquina, boolean aire,
			boolean calefaccion, boolean extintores, boolean libroreclamaciones,boolean libroruta, boolean botiquin, 
			boolean martillos, boolean correas, boolean sepiolita){
        FormularioDao daoprueba = new FormularioDao();
        Formulario formulario = new Formulario(nombre,nivelaceite,nivelagua,nivelcarburante,estadoruedas,carroceria,rampa,adblue,maquina,
        	aire,calefaccion, extintores,libroreclamaciones,libroruta, botiquin,martillos, correas, sepiolita);
        daoprueba.addFormulario(formulario);
	}
	//updating an existing form type.
	public void updateFormulario(String nombre, boolean nivelaceite, boolean nivelagua, boolean nivelcarburante,
			boolean estadoruedas, boolean carroceria,boolean rampa,boolean adblue, boolean maquina, boolean aire,
			boolean calefaccion, boolean extintores, boolean libroreclamaciones,boolean libroruta, boolean botiquin, 
			boolean martillos, boolean correas, boolean sepiolita){
		FormularioDao daoprueba = new FormularioDao();
		Formulario formulario = new Formulario(nombre,nivelaceite,nivelagua,nivelcarburante,estadoruedas,carroceria,rampa,adblue,maquina,
	        	aire,calefaccion, extintores,libroreclamaciones,libroruta, botiquin,martillos, correas, sepiolita);
		daoprueba.updateFormulario(formulario);
	}
	//deleting a form from the db.
	public void deleteFormulario(String nombre){
		FormularioDao dao = new FormularioDao();
		dao.deleteFormulario(nombre);
	}
	//Getting a string JSON with the list of forms that exist in the db.
	public String getAllFormulario(){
		FormularioDao dao = new FormularioDao();
		final Gson gson = new Gson();
		List<Formulario> formulario = dao.getAllFormularios();
		String representacionJSON = gson.toJson(formulario, new TypeToken<ArrayList<Formulario>>(){}.getType());
		return representacionJSON;
	}
}
