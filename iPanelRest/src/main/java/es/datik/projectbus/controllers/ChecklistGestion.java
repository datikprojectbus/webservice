package es.datik.projectbus.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;

import es.datik.projectbus.ddbb.classes.ChecklistDao;
import es.datik.projectbus.ddbb.classes.TurnoDao;
import es.datik.projectbus.ddbb.classes.UserDao;
import es.datik.projectbus.ddbb.classes.VehiculoDAO;
import es.datik.projectbus.ddbb.domain.Checklist;
import es.datik.projectbus.ddbb.domain.Turno;
import es.datik.projectbus.ddbb.domain.Users;
import es.datik.projectbus.ddbb.domain.Vehiculo;
import es.datik.projectbus.dto.Checklist_class;

public class ChecklistGestion {
	
	private static ChecklistGestion instance = null;
    
	public static ChecklistGestion getInstance() {
        if (instance == null ) {
            instance = new ChecklistGestion();
        }
        return instance;
    }
	// Get info about a vehicle through the registration, a starting Date and an end Date.
	public List<String> getInfo(String matricula, String startDate, String endDate) throws ParseException{
        
		ChecklistDao dao2 = new ChecklistDao();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date minDate = formatter.parse(startDate);
        Date newDate = formatter.parse(endDate);
        Calendar cal = Calendar.getInstance(); 
        cal.setTime(newDate);
        cal.add(Calendar.HOUR_OF_DAY, 24);
        Date maxDate = cal.getTime();
        List<Checklist> checklist = dao2.getChecklistbetween2Dates(matricula, minDate, maxDate);
		final Gson gson = new Gson();
		int size = checklist.size();
		List<String> listjson = new ArrayList<String>();
		for (int i=0; i<size; i++){
			Checklist prueba = checklist.get(i);
			int id = prueba.getIdchecklist();
			Date time = prueba.getFecha();
	        String turno = prueba.getTurno().getIdturno();
	        int id_user = prueba.getUsers().getId();
	        String ruedas = prueba.getEstadoruedas();
	        String adblue = prueba.getAdblue();
	        String aire = prueba.getAireAcondicionado();
	        String botiquin = prueba.getBotiquin();
	        String calefaccion = prueba.getCalefaccion();
	        String carroceria = prueba.getCarroceria();
	        String correas = prueba.getCorreas();
	        String extintores = prueba.getExtintores();
	        String libroreclamaciones = prueba.getLibroreclamaciones();
	        String libroruta =  prueba.getLibroruta();
	        String maquina = prueba.getMaquinacobro();
	        String martillos = prueba.getMartillos();
	        String aceite = prueba.getNivelaceite();
	        String agua = prueba.getNivelagua();
	        String carburante = prueba.getNivelcarburante();
	        String rampa = prueba.getRampaminusvalidos();
	        String sepiolita = prueba.getSepiolita();
	        Checklist_class clase = new Checklist_class(id,matricula,time,turno,id_user, ruedas,adblue,aire,botiquin,
	                calefaccion,carroceria, correas,extintores,libroreclamaciones,libroruta,maquina,martillos,aceite,agua,
	                carburante, rampa, sepiolita);
			String jsonstring = gson.toJson(clase);
			listjson.add(jsonstring);
		}
        return listjson;
	}
	//adding a checklist to the db
	public void addChecklist(String matricula, String username, String turno,
			String nivelaceite, String nivelagua, String nivelcarburante,
			String estadoruedas, String carroceria, String rampa,
			String adblue, String maquina, String aire, String calefaccion,
			String extintores, String libroreclamaciones, String libroruta,
			String botiquin, String martillos, String correas, String sepiolita) {
		
		ChecklistDao dao = new ChecklistDao();
		VehiculoDAO dao2 = new VehiculoDAO();
		TurnoDao dao3 = new TurnoDao();
		UserDao dao4 = new UserDao();
        Vehiculo vehiculobject = dao2.getElementByMatricula(matricula);
        Turno turnobject = dao3.getTurnoById(turno);
        Users userobject = dao4.getUserByUsername(username);
        Calendar calendar = Calendar.getInstance();
        Date dt = calendar.getTime();
        System.out.printf(nivelaceite, nivelagua, nivelcarburante, estadoruedas, carroceria, rampa, adblue, maquina, aire, calefaccion,extintores,
				libroreclamaciones, libroruta, botiquin, martillos, correas);
		Checklist checklist = new Checklist(userobject, vehiculobject, turnobject, dt,nivelaceite, nivelagua, 
				nivelcarburante, estadoruedas, carroceria, rampa, adblue, maquina, aire, calefaccion,extintores,
				libroreclamaciones, libroruta, botiquin, martillos, correas,sepiolita);

        dao.addChecklist(checklist);
	}
	//deleting a checklist from the db
	public void deleteChecklist(int id){
		ChecklistDao dao = new ChecklistDao();
		dao.deleteChecklist(id);
	}
}
