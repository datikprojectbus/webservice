/**
 * 
 */
package es.datik.projectbus.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import com.google.gson.Gson;

/**
 * @author elglarig
 *
 */
public class EncodeBase64 {
	public static String serialize(Object object) throws IOException {
	    ByteArrayOutputStream byteaOut = new ByteArrayOutputStream();
	    GZIPOutputStream gzipOut = null;
	    //Base64OutputStream base64 = null;
	    try {
	    	gzipOut = new GZIPOutputStream(new Base64OutputStream(byteaOut));
	        gzipOut.write(new Gson().toJson(object).getBytes("UTF-8"));
	    	// Sin comprimir
	        //base64 = new Base64OutputStream(byteaOut);
	    	//base64.write(new Gson().toJson(object).getBytes("UTF-8"));
	    } finally {
	        if (gzipOut != null) try { gzipOut.close(); } catch (IOException logOrIgnore) {}
	    	//if (base64 != null) try { base64.close(); } catch (IOException logOrIgnore) {}
	    }
	    String resultado = new String(byteaOut.toByteArray());
	    resultado = resultado.replaceAll("(\\r|\\n)", "");
	    return resultado;
	}

	public static <T> T deserialize(String string, Type type) throws IOException {
	    ByteArrayOutputStream byteaOut = new ByteArrayOutputStream();
	    GZIPInputStream gzipIn = null;
	    //Base64InputStream base64 = null;
	    try {
	        gzipIn = new GZIPInputStream(new Base64InputStream(
	        		new ByteArrayInputStream(string.getBytes("UTF-8"))));
	        for (int data; (data = gzipIn.read()) > -1;) {
	            byteaOut.write(data);
	        }
	        // Sin comprimir
	    	//base64 = new Base64InputStream(new ByteArrayInputStream(string.getBytes("UTF-8")));
	        //for (int data; (data = base64.read()) > -1;) {
	        //    byteaOut.write(data);
	        //}
	        
	    } finally {
	        if (gzipIn != null) try { gzipIn.close(); } catch (IOException logOrIgnore) {}
	    	//if (base64 != null) try { base64.close(); } catch (IOException logOrIgnore) {}
	    }
	    return new Gson().fromJson(new String(byteaOut.toByteArray()), type);
	}
}
