package es.datik.projectbus.ddbb.classes;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import es.datik.projectbus.ddbb.domain.Formulario;
import es.datik.projectbus.ddbb.domain.Users;
import es.datik.projectbus.ddbb.resources.HibernateUtil;

public class FormularioDao {
	
	Logger logger = Logger.getLogger("FormularioDao");
	
	public void addFormulario(Formulario formulario) {
        Session session = null; 
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(formulario);
            session.getTransaction().commit();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"addFormulario: " + e.getClass()+ ":" + e.getMessage());
        	session.close();
            throw e;
        }
    }

    public void deleteFormulario(String nombre){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Formulario formulario = (Formulario)session.createCriteria(Formulario.class).add(Restrictions.eq("nombre", nombre)).uniqueResult();
            
            session.delete(formulario);
            session.getTransaction().commit();
            
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Deleting formulario: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }

    public void updateFormulario(Formulario formulario) {
    	Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
        	session.beginTransaction();
        	
            session.update(formulario);
            session.getTransaction().commit();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Update formulario options: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }

    public List<Formulario> getAllFormularios() {
       	List<Formulario> listformularios = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            
            listformularios = session.createCriteria(Formulario.class).addOrder(Order.asc("nombre")).list(); 
            session.close();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"Read all formularios: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;}
        return listformularios;
    }
}
