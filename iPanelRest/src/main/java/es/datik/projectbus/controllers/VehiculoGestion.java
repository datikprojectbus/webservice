package es.datik.projectbus.controllers;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;


import es.datik.projectbus.ddbb.classes.VehiculoDAO;
import es.datik.projectbus.ddbb.domain.Vehiculo;
import es.datik.projectbus.dto.Vehiculo_class;

public class VehiculoGestion {
	private static VehiculoGestion instance = null;
    
	public static VehiculoGestion getInstance() {
        if (instance == null ) {
            instance = new VehiculoGestion();
        }
        return instance;
    }
	//Adding a vehicle to the db.
	public void addVehiculo (String matricula, String bodywork, String modelo, String motor){
		VehiculoDAO dao = new VehiculoDAO();
		Vehiculo vehiculo = new Vehiculo();
		vehiculo.setMatricula(matricula);
		vehiculo.setBodywork(bodywork);
		vehiculo.setModelo(modelo);
		vehiculo.setMotor(motor);
		dao.addVehiculo(vehiculo);
	}
	//Reading all the vehicles that are stored in the db.
	public List<String> readAllVehiculos(){
		VehiculoDAO dao = new VehiculoDAO();
		final Gson gson = new Gson();
		List<Vehiculo> vehiculos = dao.readAll();
		int size = vehiculos.size();
		List<String> listjson = new ArrayList<String>();
		for (int i=0; i<size; i++)
		{	Vehiculo prueba = vehiculos.get(i);
			String matricula = prueba.getMatricula();
			String bodywork = prueba.getBodywork();
			String modelo = prueba.getModelo();
			String motor = prueba.getMotor();
			Vehiculo_class clase = new Vehiculo_class(matricula,bodywork,modelo,motor);
			String jsonstring = gson.toJson(clase);
			listjson.add(jsonstring);
		}
		return listjson;
	}
	//Deleting a vehicle
	public void deleteVehiculo(String matricula){
		VehiculoDAO dao = new VehiculoDAO();
		dao.deleteVehiculo(matricula);
	}
}
