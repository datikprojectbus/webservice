package es.datik.projectbus.rest.prueba;

import java.io.IOException;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import es.datik.projectbus.dto.DemoAuthenticator;
import es.datik.projectbus.dto.HTTPHeaderNames;

@Provider
@PreMatching
public class RestRequestFilter implements ContainerRequestFilter {
	
	private final static Logger log = Logger.getLogger( RestRequestFilter.class.getName() );
	@Override
	public void filter(ContainerRequestContext request) throws IOException {
		String path = request.getUriInfo().getPath();
        log.info( "Filtering request path: " + path );
        
        // IMPORTANT!!! First, Acknowledge any pre-flight test from browsers for this case before validating the headers (CORS stuff)
        if ( request.getRequest().getMethod().equals( "OPTIONS" ) ) {
            request.abortWith( Response.status( Response.Status.OK ).build() );
 
            return;
        }
        //For any other methods besides login, the authToken must be verified
        if ( !path.startsWith( "proxyresource/login" ) ) {
            String authToken = request.getHeaderString( HTTPHeaderNames.AUTH_TOKEN );
    		DemoAuthenticator authenticator = DemoAuthenticator.getInstance();
            // if it isn't valid, just kick them out. OR AUTH TOKEN LIFE EXPIRED!
            if ( !authenticator.isAuthTokenValid(authToken)) {
            	request.abortWith( Response.status( Response.Status.UNAUTHORIZED ).build() );
            }
            if( authenticator.isTokenExpired(authToken)){
            	//Response.STATUS.TYPEResponse.Status.UNAUTHORIZED ).entity( data.toString() ).build();
   			 	JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
   			 	jsonObjBuilder.add( "tokenexpired", "La sessión ha expirado" );
   			 	JsonObject data = jsonObjBuilder.build();
            	request.abortWith( Response.status(Response.Status.UNAUTHORIZED ).entity(data.toString()).build());
            }
        }
    }
}
