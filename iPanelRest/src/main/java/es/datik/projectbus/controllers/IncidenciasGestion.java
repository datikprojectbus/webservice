package es.datik.projectbus.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;

import es.datik.projectbus.ddbb.classes.IncidenciasDao;
import es.datik.projectbus.ddbb.classes.TurnoDao;
import es.datik.projectbus.ddbb.classes.UserDao;
import es.datik.projectbus.ddbb.classes.VehiculoDAO;
import es.datik.projectbus.ddbb.domain.Incidencia;
import es.datik.projectbus.ddbb.domain.Turno;
import es.datik.projectbus.ddbb.domain.Users;
import es.datik.projectbus.ddbb.domain.Vehiculo;
import es.datik.projectbus.dto.Incidencia_class;

public class IncidenciasGestion {

	
	private static IncidenciasGestion instance = null;
    
	public static IncidenciasGestion getInstance() {
        if (instance == null ) {
            instance = new IncidenciasGestion();
        }
        return instance;
    }
	//adding incidences to the db.
	public void addincidencias(String matricula,String username,String turno,String tipo,String descripcion){
		IncidenciasDao dao = new IncidenciasDao();
        Incidencia incidencia = new Incidencia();
        VehiculoDAO dao2 = new VehiculoDAO();
        Vehiculo vehiculo = dao2.getElementByMatricula(matricula);
        vehiculo.setMatricula(matricula);
        incidencia.setVehiculo(vehiculo);
        TurnoDao dao4 = new TurnoDao();
        Turno turn = dao4.getTurnoById(turno);
        incidencia.setTurno(turn);
        UserDao dao3 = new UserDao();
        Users user = dao3.getUserByUsername(username);
        incidencia.setUsers(user);
        Calendar calendar = Calendar.getInstance();
        Date dt = calendar.getTime();
        incidencia.setDate(dt);
        incidencia.setTipo(tipo);
        incidencia.setDescripcion(descripcion);
        dao.addIncidencia(incidencia);
		
	}
	//Getting a list of all the incidences from the db.
	public List<String> getIncidencias(){
		IncidenciasDao dao = new IncidenciasDao();
		final Gson gson = new Gson();
		List<Incidencia> incidencias = dao.getAllIncidences();
		int size = incidencias.size();
		List<String> listjson = new ArrayList<String>();
		for (int i=0; i<size; i++)
		{	Incidencia prueba = incidencias.get(i);
			int id_incidencia = prueba.getIdIncidencia();
			String turno = prueba.getTurno().getIdturno();
			int id = prueba.getUsers().getId();
			String matricula = prueba.getVehiculo().getMatricula();
			Date fecha = prueba.getDate();
			String tipo = prueba.getTipo();
			String descripcion = prueba.getDescripcion();
			Incidencia_class clase = new Incidencia_class(id_incidencia,matricula,fecha,id, turno,tipo,descripcion);
			String jsonstring = gson.toJson(clase);
			listjson.add(jsonstring);

		}
		return listjson;
	}
	//Getting only the mecanico type incidences.
	public List<String> getIncidenciasMecanicas(){
		IncidenciasDao dao = new IncidenciasDao();
		final Gson gson = new Gson();
		List<Incidencia> incidencias = dao.getAllIncidencesMecanicas();
		int size = incidencias.size();
		List<String> listjson = new ArrayList<String>();
		for (int i=0; i<size; i++)
		{	Incidencia prueba = incidencias.get(i);
			int id_incidencia = prueba.getIdIncidencia();
			String turno = prueba.getTurno().getIdturno();
			int id = prueba.getUsers().getId();
			String matricula = prueba.getVehiculo().getMatricula();
			Date fecha = prueba.getDate();
			String tipo = prueba.getTipo();
			String descripcion = prueba.getDescripcion();
			Incidencia_class clase = new Incidencia_class(id_incidencia,matricula,fecha,id, turno,tipo,descripcion);
			String jsonstring = gson.toJson(clase);
			listjson.add(jsonstring);

		}
		return listjson;
	}
	//deleting a incidence by its id.
	public void deleteIncidencia(int id){
		IncidenciasDao dao = new IncidenciasDao();
		dao.deleteIncidencia(id);
	}
}
