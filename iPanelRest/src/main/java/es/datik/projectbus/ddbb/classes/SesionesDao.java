package es.datik.projectbus.ddbb.classes;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import es.datik.projectbus.ddbb.domain.Sesiones;
import es.datik.projectbus.ddbb.resources.HibernateUtil;

public class SesionesDao {

	Logger logger = Logger.getLogger("SesionesDao");
    
	public void addUserSesion(Sesiones usersesion) {
        Session session = null; 
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(usersesion);
            session.getTransaction().commit();
        } catch (Exception e) {
        	logger.log(Level.SEVERE,"addUserSesion: " + e.getClass()+ ":" + e.getMessage());
        	session.close();
            throw e;
        }
    }
    public void deleteUserSesion(int id){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction(); //"id" or "userid"?
            Sesiones usersesion = (Sesiones)session.createCriteria(Sesiones.class).add(Restrictions.eq("id", id)).uniqueResult();
	        System.out.println("Testing users");
            session.delete(usersesion);
            session.getTransaction().commit();
            
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Delete user sesion: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }
    //used to update the token life or to change the token (old token expired)
    public void updateUserSesion(Sesiones usersesion) {
    	Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
        	session.beginTransaction();
        	
            session.update(usersesion);
            session.getTransaction().commit();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Update user sesion: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
    }
    public Sesiones getUserById(int id) {
        Sesiones usersesion = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
			usersesion = (Sesiones)session.createCriteria(Sesiones.class).add(Restrictions.eq("id", id )).uniqueResult();
			session.close();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Get user by id: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
        return usersesion;
    }
    public Sesiones getUserByToken(String token) {
        Sesiones usersesion = null;
        Session session = null;
        try {
        	session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
			usersesion = (Sesiones)session.createCriteria(Sesiones.class).add(Restrictions.eq("token", token )).uniqueResult();
			session.close();
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Get user by id: " + e.getClass()+ ":" + e.getMessage());
			session.close();
			throw e;
        }
        return usersesion;
    }
}
