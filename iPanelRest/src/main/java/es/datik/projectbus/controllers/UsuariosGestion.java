package es.datik.projectbus.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.LoginException;

import com.google.gson.Gson;

import es.datik.projectbus.ddbb.classes.UserDao;
import es.datik.projectbus.ddbb.domain.Users;
import es.datik.projectbus.dto.User_class;
import es.datik.projectbus.utils.Security;

public class UsuariosGestion {

	private static UsuariosGestion userinstance = null;
	
    public static UsuariosGestion getInstance() {
        if ( userinstance == null ) {
            userinstance = new UsuariosGestion();
        }
        return userinstance;
    }
    //This method adds or modifies information in the db.
    public void gestionusuarios(String name, String surname, String username, String privilege, String email,String password ) throws LoginException {
    	UserDao dao = new UserDao();
    	Users user = dao.getUserByUsername(username);
    	//Username exists in the db, so we are editing it.
    	if (user != null && user.getUsername().equals(username)){
    		user.setPrivilege(privilege);
    		user.setEmail(email);
            Security security = new Security();
            String hash = security.md5(password);
    		user.setPassword(hash);
    		dao.updateUser(user);
    	}
    	else {
    	Users newuser = new Users();
    	newuser.setName(name);
    	newuser.setSurname(surname);
    	newuser.setUsername(username);
    	newuser.setPrivilege(privilege);
    	newuser.setEmail(email);
    	Security security = new Security();
        String hash = security.md5(password);
    	newuser.setPassword(hash);
    	dao.addUser(newuser);
    	}
    }
    //Getting a List of all the users.
    public List<String> GetAllUsers(){
		UserDao dao = new UserDao();
		final Gson gson = new Gson();
		List<Users> usuarios = dao.getAllUsers();
		int size = usuarios.size();
		List<String> listjson = new ArrayList<String>();
		for (int i=0; i<size; i++)
		{	Users prueba = usuarios.get(i);
			int id = prueba.getId();
			String name = prueba.getName();
			String surname = prueba.getSurname();
			String username = prueba.getUsername();
			String privilege = prueba.getPrivilege();
			String email = prueba.getEmail();
			User_class clase = new User_class(id,name,surname,username,privilege,email);
			String jsonstring = gson.toJson(clase);
			listjson.add(jsonstring);
		}
		return listjson;
    }
    //Delete a specific (id) user.x
    public void DeleteUser (int id){
    	UserDao dao = new UserDao();
    	dao.deleteUser(id);
    }
}
