package es.datik.projectbus.dto;


public class User_class {
	private int id;
	private String name;
	private String surname;
	private String username;
	private String privilege;
	private String email;

	public User_class(int id, String name, String surname, String username, String privilege, String email){
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.privilege = privilege;
		this.email = email;
	}
}

