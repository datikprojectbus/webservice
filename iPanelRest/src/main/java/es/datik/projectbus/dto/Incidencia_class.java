package es.datik.projectbus.dto;

import java.util.Date;

public class Incidencia_class {
	private int id_incidencia;
	private String matricula;
	private Date fecha;
	private int id;
	private String turno;
	private String tipo;
	private String descripcion;

	public Incidencia_class(int id_incidencia, String matricula, Date fecha, int id, String turno, String tipo, String descripcion) {
		this.id_incidencia = id_incidencia;
		this.turno = turno;
		this.id = id;
		this.matricula = matricula;
		this.fecha = fecha;
		this.tipo = tipo;
		this.descripcion = descripcion;
	}
}
