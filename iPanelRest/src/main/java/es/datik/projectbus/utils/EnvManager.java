package es.datik.projectbus.utils;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

import org.apache.log4j.Logger;



/**
 * Clase que devuelve las variables de entorno relacionadas con 
 * la aplicación iPanel
 * @author elglarig
 *
 */
public class EnvManager {
	
	static private Logger log4j = Logger.getLogger("EnvManager");
	
	public static Properties getProperties(String propFile) throws FileNotFoundException, IOException{
		Properties properties = new Properties();
		try{
			properties.load(getInputPropertiesStream(getFileName(propFile)));
		}catch(FileNotFoundException e){
			properties = System.getProperties();
			log4j.error("#22#500# File not found: " + propFile + ". Using system properties.",e);
		} catch (IOException e) {
			properties = System.getProperties();
			log4j.error("#22#500# Error reading file: " + propFile + ". Using system properties.",e);
		}catch(Exception e){
			// 	No se han podido cargar las propiedades del fichero "propFile". 
			// Se tomarón los valores por defecto.
			String texto = MessageFormat.format("#22#500# No se han podido cargar las propiedades del fichero \"{0}\". Se tomar�n los valores por defecto.", 
			propFile);
			log4j.error(texto + " " + e);
		}
		return properties;
	}
	
	private static InputStream getInputPropertiesStream(String file){
		try{	
			return EnvManager.class.getClassLoader().getResourceAsStream(file);
		}catch(Exception e){
			log4j.error("#22#500# Error al cagar el archivo de propiedades : " + file + " con el error: " + e);
			return null;
		}
	}
	
	public static Properties getProperties(String initPath, String propFile) throws FileNotFoundException, IOException{
		Properties properties = new Properties();
		try{
			properties.load(getInputPropertiesStream(getFileName(initPath, propFile)));
		}catch(FileNotFoundException e){
			properties = System.getProperties();
			log4j.error("#22#500# File not found: " + propFile + ". Using system properties.", e);
		} catch (IOException e) {
			properties = System.getProperties();
			log4j.error("#22#500# Error reading file: " + propFile + ". Using system properties.", e);
		}catch(Exception e){
			// 	No se han podido cargar las propiedades del fichero "propFile". 
			// Se tomarón los valores por defecto.
			String texto = MessageFormat.format("#22#500# No se han podido cargar las propiedades del fichero \"{0}\". Se tomar�n los valores por defecto.", 
			propFile);
			log4j.error(texto + " " + e);
		}
		return properties;
	}
	
	/**
	 * Obtiene el contenido del fichero properties
	 * @param propFile
	 * @param defaults
	 * @return Devuelve el contenido del fichero properties
	 */
	public static Properties getProperties(String propFile, Properties defaults){
		Properties properties = new Properties(defaults);
		try{
			properties.load(getInputPropertiesStream(getFileName(propFile)));
		}catch(FileNotFoundException e){
				properties = System.getProperties();
				log4j.error("#22#500# File not found: " + propFile + ". Using system properties.",e);
		} catch (IOException e) {
				properties = System.getProperties();
				log4j.error("#22#500# Error reading file: " + propFile + ". Using system properties.",e);
		}catch(Exception e){
			// No se han podido cargar las propiedades del fichero "propFile". 
			// Se tomarón los valores por defecto.
			String texto = MessageFormat.format("#22#500# No se han podido cargar las propiedades del fichero \"{0}\". Se tomarón los valores por defecto.", 
					propFile);
			log4j.error(texto + " " + e);
		}
		return properties;
	}
	
	/**
	 * Obtiene el nombre del archivo
	 * @param name nombre del archivo al cual obtener las propiedades
	 * @return Devuelve el path al fichero properties
	 */
	private static String getFileName(String name){
		String fName = getPropertiesPath() + name;		
		if(!name.endsWith(".properties")){
			fName += ".properties";
		}
		return fName;
	}
	
	/**
	 * Obtiene el nombre del archivo y donde se encuentra
	 * @param initPath donde se encuentra el archivo de las propiedades
	 * @param name nombre del archivo
	 * @return direccion completa donde esta el archivo
	 */
	private static String getFileName(String initPath, String name){
		String fName = getPropertiesPath(initPath) + name;
		if(!name.endsWith(".properties")){
			fName += ".properties";
		}
		return fName;
	}
	
	/**
	 * Devuelve el valor donde tiene que encontrar  donde estan las propiedades
	 * @return Valor del lugar donde se encuentra las propiedades
	 */
	public static String getPropertiesPath(){
		String path = "properties" + File.separator;
		//String path = System.getenv("IPANEL_PROPERTIES");
		//if(path == null){
		//	path = "properties";
		//}
		//if(!path.endsWith(File.separator)){
		//	path += File.separator;
		//}
		return path;
	}
	
	/**
	 * Devuelve el valor de la variable de entorno MUSIK_PROPERTIES
	 * @return Valor de la variable IPANEL_PROPERTIES
	 */
	public static String getPropertiesPath(String initPath){
		String path = System.getenv("IPANEL_PROPERTIES");
		if(path == null){
			path = initPath + "/properties";
		}
		if(!path.endsWith(File.separator)){
			path += File.separator;
		}
		return path;
	}


}
