package es.datik.projectbus.dto;

public class Vehiculo_class {
	private String matricula;
	private String bodywork;
	private String model;
	private String motor;
	
	public Vehiculo_class(String matricula, String bodywork, String model, String motor){
		this.matricula = matricula;
		this.bodywork = bodywork;
		this.model = model;
		this.motor = motor;
	}
}
